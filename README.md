# code-sections

A simple Vim plugin, providing functions to replace `]]`, `][`, `[[` and `[]`
in braceless languages such as Python.


## `codeSections#moveToNextBlockStart` (`]]`)

Before:

    Lorem<cursor>
    ipsum

    dolor


    sit
    amet

After:

    Lorem
    ipsum

    dolor


    <cursor>sit
    amet


## `codeSections#moveToNextBlockEnd` (`][`)

Before:

    Lorem<cursor>
    ipsum

    dolor


    sit
    amet

After:

    Lorem
    ipsum

    <cursor>dolor


    sit
    amet


## `codeSections#moveToPreviousBlockStart` (`[[`)

Before:

    Lorem
    ipsum

    dolor


    sit
    amet<cursor>

After:

    Lorem
    ipsum

    dolor


    <cursor>sit
    amet


## `codeSections#moveToPreviousBlockEnd` (`[]`)

Before:

    Lorem
    ipsum

    dolor


    sit
    amet<cursor>

After:

    Lorem
    ipsum

    <cursor>dolor


    sit
    amet

