" block ::= ( <blank line>{2,} | <beginning of file> )
"           <non-blank line>+
"           ( <blank line>{2,} | <end of file> )

function! codesections#moveToNextBlockStart()
    call search('\(^$\n\)\{2,}.', 'esW')
endfunction

function! codesections#moveToPreviousBlockStart()
    call search('\(\(^$\n\)\{2,}\|\%^\).', 'besW')
endfunction

function! codesections#moveToNextBlockEnd()
    call search('^.\+\(\n\(^$\n\)\{2,}\|\n*\%$\)', 'sW')
endfunction

function! codesections#moveToPreviousBlockEnd()
    call search('^.\+$\n\(^$\n\)\{2,}.', 'bsW')
endfunction


function! codesections#bufferDefaultMappings()
    xmap <buffer><silent><expr> ]] codesections#moveToNextBlockStart()
    nmap <buffer><silent> ]] :call codesections#moveToNextBlockStart()<CR>
    vmap <buffer><silent> ]] :<C-U> exe "normal! gv"\|call codesections#moveToNextBlockStart()<CR>
    xmap <buffer><silent><expr> [[ codesections#moveToPreviousBlockStart()
    nmap <buffer><silent> [[ :call codesections#moveToPreviousBlockStart()<CR>
    vmap <buffer><silent> [[ :<C-U> exe "normal! gv"\|call codesections#moveToPreviousBlockStart()<CR>
    xmap <buffer><silent><expr> ][ codesections#moveToNextBlockEnd()
    nmap <buffer><silent> ][ :call codesections#moveToNextBlockEnd()<CR>
    vmap <buffer><silent> ][ :<C-U> exe "normal! gv"\|call codesections#moveToNextBlockEnd()<CR>
    xmap <buffer><silent><expr> [] codesections#moveToPreviousBlockEnd()
    nmap <buffer><silent> [] :call codesections#moveToPreviousBlockEnd()<CR>
    vmap <buffer><silent> [] :<C-U> exe "normal! gv"\|call codesections#moveToPreviousBlockEnd()<CR>
endfunction

function! codesections#globalDefaultMappings()
    xmap <silent><expr> ]] codesections#moveToNextBlockStart()
    nmap <silent> ]] :call codesections#moveToNextBlockStart()<CR>
    vmap <silent> ]] :<C-U> exe "normal! gv"\|call codesections#moveToNextBlockStart()<CR>
    xmap <silent><expr> [[ codesections#moveToPreviousBlockStart()
    nmap <silent> [[ :call codesections#moveToPreviousBlockStart()<CR>
    vmap <silent> [[ :<C-U> exe "normal! gv"\|call codesections#moveToPreviousBlockStart()<CR>
    xmap <silent><expr> ][ codesections#moveToNextBlockEnd()
    nmap <silent> ][ :call codesections#moveToNextBlockEnd()<CR>
    vmap <silent> ][ :<C-U> exe "normal! gv"\|call codesections#moveToNextBlockEnd()<CR>
    xmap <silent><expr> [] codesections#moveToPreviousBlockEnd()
    nmap <silent> [] :call codesections#moveToPreviousBlockEnd()<CR>
    vmap <silent> [] :<C-U> exe "normal! gv"\|call codesections#moveToPreviousBlockEnd()<CR>
endfunction

